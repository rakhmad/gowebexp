# TODO


## forms

### csrf protection add a secret token in the cookie and ensure that the token
is there when posted.

### Form Validation and error reporting

The [form to add a page](http://127.0.0.1:8080/pages/) should validate the
presence on an non empty name and check that the slug is unique. Errors should
be reported appropriately.


## Add user management

## Implement a persistent storage engine that keep for User and Page

## Add few attributes to the Page object

* state: WIP, Published


