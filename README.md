# Gowebexp

As the name implies this repository contains a toy webapp that I have built
while experimenting with GO.
The webapp does not persist anything in database or disk so
everything will be lost every time you stop the server. At some point
I may add a presidency layer but this not the part I was interested to explore.

## Goal

The goal for this app is to learn about GO in the context a building a webapp
bigger than the canonical "Hello World".

Here it is few features that are exhibited in Gowebexp:

* regex based URL dispatching
* URL reversing
* session
* form validation
* template inheritance
* Cross site request forgery for the form (csrf_token)

## Installation

Gowebexp should be simple to install because it is `go get -able`

1. Create a workspace that you will use as test bed

```
mkdir workspace_gowebexp
cd gowebexp
```

1. `Gowebexp` relies on some environ variables to find the templates and the
static files. Save the following bash script in a file called `set_env.sh`
in `workspace_gowebexp`:

```
###############################
# GO environ variables
###############################
export GOROOT=$HOME/go
export GOARCH=amd64
export GOOS=linux
export PATH=$GOROOT/bin:$PATH
export GOPATH=`pwd`:$GOPATH

################################
# Environ variables for Gowebexp
################################
GOWEBEXP_DIR="src/bitbucket.org/yml/gowebexp"

export GOWEBEXP_STATIC=`pwd`/$GOWEBEXP_DIR"/web/static"
export GOWEBEXP_TEMPLATES=`pwd`/$GOWEBEXP_DIR"/web/templates"
```
1. Download `Gowebexp` from bitbucket using `go get` this will also pull all
the dependecies require to run it

```
go get bitbucket.org/yml/gowebexp
```

1. Install `Gowebexp` using `go install`

```
go install bitbucket.org/yml/gowebexp
```

1. Run it

```
./bin/gowebexp
```

If you want to take advantage of multiple cores you can set the
`GOMAXPROCS` environ variable

```
GOMAXPROCS=4 ./bin/gowebexp
```

