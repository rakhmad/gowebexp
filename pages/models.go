package pages

import (
	"errors"
	"github.com/russross/blackfriday"
	"html/template"
)

const (
	errorMsgRequiredField = "This field is required"
)

var (
	ValidationError = errors.New("Validation Error")
)

type Page struct {
	Name    string
	Slug    string
	Content string
}

// DISCUSS: It feels weird to push |safe logic heere I would rahter keep it in the
// template
func (p Page) RenderedContent() template.HTML {
	return template.HTML(string(blackfriday.MarkdownCommon([]byte(p.Content))))
}

func (p *Page) Validate() (validationErrors map[string]string, err error) {
	ValidationErrors := make(map[string]string)
	if p.Name == "" {
		ValidationErrors["Name"] = errorMsgRequiredField
		err = ValidationError
	}
	if p.Slug == "" {
		ValidationErrors["Slug"] = errorMsgRequiredField
		err = ValidationError
	}
	return ValidationErrors, err
}
